#include <iostream>

struct List {
    int data;
    struct List* next;
    struct List* prev;
};

struct List* init(List* l, int a) {
    l->data = a;
    l->next = l;
    l->prev = l;
    return(l);
}
struct List* add(List* l, int num) {
    struct List* temp, * p;
    temp = (struct List*)malloc(sizeof(List));
    p = l->next;
    l->next = temp;
    temp->data = num;
    temp->next = p;
    temp->prev = l;
    p->prev = temp;
    return(temp);
}

struct List* del(List* l) {
    struct List* prev, * next;
    prev = l->prev;
    next = l->next;
    prev->next = l->next;
    next->prev = l->prev;
    free(l);
    return(prev);
}

void echo(List* l) {
    struct List* p;
    p = l;
    do {
        printf("%d ", p->data);
        p = p->next;
    } while (p != l);
}

void echor(List* l) {
    struct List* p;
    p = l;
    do {
        p = p->prev;
        printf("%d ", p->data);
    } while (p != l);
}
int main() {
    List* l = new List;
    init(l, 10);
    add(l, 11);
    add(l->next, 12);
    add(l->next->next, 9);

    echo(l);
    std::cout << std::endl;

    del(l->next);
    echor(l);
    std::cout << std::endl;

    echor(l);
}
